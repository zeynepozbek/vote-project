let linkList = [
    {
        name: 'Hacker News',
        link: 'https://news.ycombinator.com/',
        vote: 6
    },
    {
        name: 'Product Hunt',
        link: 'https://producthunt.com',
        vote: 4
    },
    {
        name: 'Reddit',
        link: 'https://www.reddit.com',
        vote: 4
    },
    {
        name: 'Quora',
        link: 'https://www.quora.com',
        vote: 3
    }
];


import Vue from 'vue'
import { shallowMount, mount } from "@vue/test-utils";
import CreateLink from "@/components/CreateLink.vue";

const $router = {
    replace: jest.fn()
};

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

test("array sort (Less Votes(A->Z))", () => {

    var list = linkList
    function compare(a, b) {
        if (a.vote < b.vote)
            return -1;
        if (a.vote > b.vote)
            return 1;
        return 0;
    }
    list.sort(compare)

    expect(list[3].name).toBe('Hacker News');

    console.log('OK - array sort (Less Votes(A->Z))')
});

test("array sort (Most Voted(Z->A))", () => {

    var list = linkList
    function compare(a, b) {
        if (a.vote < b.vote)
            return -1;
        if (a.vote > b.vote)
            return 1;
        return 0;
    }
    list.sort(compare)

    list = list.reverse()

    expect(list[3].name).toBe('Quora');

    console.log('OK - array sort (Most Voted(Z->A)))')
});

fdescribe("CreateLink.vue", () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallowMount(CreateLink, {
            mocks: {
                $router
            },
            data() {
                return {
                    name: "test",
                    link: "https://google.com",
                    vote: 0
                };
            }
        });
    });

    it("renders", () => {
        expect(wrapper.exists()).toBe(true);
    });

});

describe('CreateLink.vue', () => {
    it('validates model on button click', () => {
        const handleSubmit = jest.fn();
        const wrapper = mount(
            <CreateLink handleSubmit={handleSubmit}/>
        );
        const instance = wrapper.instance();
        const submitBtn = app.find('#submit')
        submitBtn.simulate('click')
        expect(handleSubmit).toHaveBeenCalled();
    });
})
