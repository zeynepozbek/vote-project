export const UPDATE_LINK_LIST = (state, payload) => {
    localStorage.setItem('linkList',JSON.stringify(payload))
    state.linkList = payload;
};
