import Vue from "vue";
import Vuex from "vuex";
import * as actions from "./actions";
import * as mutations from "./mutations";
import * as getters from "./getters";

Vue.use(Vuex);

let linkList = [
    {
        name: 'Hacker News',
        link: 'https://news.ycombinator.com/',
        vote: 6
    },
    {
        name: 'Product Hunt',
        link: 'https://producthunt.com',
        vote: 4
    },
    {
        name: 'Reddit',
        link: 'https://www.reddit.com',
        vote: 4
    },
    {
        name: 'Quora',
        link: 'https://www.quora.com',
        vote: 3
    }
];
var localCheck = localStorage.getItem('linkList')

if(!localCheck){
    localStorage.setItem('linkList',JSON.stringify(linkList))
}
else {
    linkList = JSON.parse(localCheck)
}

export const store = new Vuex.Store({
    state: {
        linkList: linkList
    },
    actions,
    mutations,
    getters
});
